const books = [
    {
        author: "Скотт Бэккер",
        name: "Тьма, что приходит прежде",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Воин-пророк",
    },
    {
        name: "Тысячекратная мысль",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Нечестивый Консульт",
        price: 70
    },
    {
        author: "Дарья Донцова",
        name: "Детектив на диете",
        price: 40
    },
    {
        author: "Дарья Донцова",
        name: "Дед Снегур и Морозочка",
    }
];


let root = document.getElementById('root')

function checkObj({author, name, price}) {
    if (!author) {
        throw new TypeError(`no author book: ${name}`)
    }

    if (!name){
        throw new TypeError(`no name book: ${author}`)
    }

    if (!price){
        throw new TypeError(`no price of book: ${author} ${name}`)
    }

    return true;
}


let addList = function (books, root) {

    const list = document.createElement('ul');

    const listItems = books.filter(item => {
        let itemIsValid = false;

        try {
            itemIsValid = checkObj(item);
        } catch(err) {
            console.error(err);
        }

        return itemIsValid;
    }).map(({author, name, price}) => {
        return `<li>Автор: ${author}; Название: ${name}; Цена: ${price}</li>`;
    });

    console.log(listItems)

    const listItemsHtml = listItems.join('');
    list.innerHTML = listItemsHtml;
    root.append(list);
}

addList(books, root)
